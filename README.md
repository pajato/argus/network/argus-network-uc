# argus-network-uc

## Description

This project implements the
[Clean Code Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
Application Business Rules" layer (aka Use Cases) for the network feature.  The project is responsible for
development of network use cases.

This project contains Argus artifacts supporting use cases (aka interactors). It exists to separate use case
concerns and support independent developability and deployment.

## License

GPL, V3, See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

For general documentation on Argus, see the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the shelf feature.

## Test Cases

### Overview

The table below identifies the adapter layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions)

| Filename Prefix  | Test Case Name                                  |
|------------------|-------------------------------------------------|
| Add              | When adding a network item, verify behavior     |
| Get              | When retrieving a network item, verify behavior |
| Modify           | When modifying a network item, verify behavior  |
| Remove           | When removing a network item, verify behavior   |
| ToggleSelection  | When a network item is toggled, verify behavior |

### Notes

The single responsibility for this project is to provide the implementation of Use Cases for Argus. In the Clean
Architecture onion diagram, this project corresponds to the Use Case layer.

The Argus Network feature use cases are:

- Manage networks
+ addNetwork(NetworkRepo, Network)
+ removeNetwork(NetworkRepo, Network)
+ toggleNetwork(Network)
+ getNetworks(NetworkRepo): List<Network>
+ getNetwork(NetworkId, NetworkRepo): Network

### Other use cases

- Manage networks, aka watch providers (add, show details, edit)
- Manage genres (select-toggle, select-multiple, show list)
- Manage watchlist (add, remove, watch)
- Show Video History List (with filtering for networks and genres; find by text)
- Show Continue Watching List (with filtering for networks and genres; find by text)
- Show Video Details (movie, tv show, tv series, tv episode)
- Show Cast List (...)
- Show Crew List (...)
- Show Episode Guest Star List (...)
- Show Cast Details (including filmography)
- Show Crew Details (including filmography)
- Show Popular Videos (fitered by genre)
- Show Top Rated Videos (filtered by genre)
- Search (top level for videos)

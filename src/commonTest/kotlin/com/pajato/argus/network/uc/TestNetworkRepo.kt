package com.pajato.argus.network.uc

import com.pajato.argus.network.core.I18nStrings.NETWORK_URI_ERROR
import com.pajato.argus.network.core.NetworkCache
import com.pajato.argus.network.core.NetworkRepo
import com.pajato.argus.network.core.NetworkRepoError
import com.pajato.argus.network.core.SelectableNetwork
import com.pajato.dependency.uri.validator.validateUri
import com.pajato.i18n.strings.StringsResource
import com.pajato.persister.jsonFormat
import com.pajato.persister.persist
import com.pajato.persister.readAndPruneData
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.net.URI

object TestNetworkRepo : NetworkRepo {
    override val cache: NetworkCache = mutableMapOf()
    private var networkUri: URI? = null

    override suspend fun injectDependency(uri: URI) {
        fun handler(cause: Throwable) { throw cause }
        validateUri(uri, ::handler)
        readAndPruneData(uri, cache, SelectableNetwork.serializer(), TestNetworkRepo::getKeyFromItem)
        networkUri = uri
    }

    override fun register(json: String) {
        val uri = networkUri ?: throw NetworkRepoError(StringsResource.get(NETWORK_URI_ERROR))
        val item = jsonFormat.decodeFromString(SelectableNetwork.serializer(), json)
        cache[item.network.id] = item
        runBlocking { launch(IO) { persist(uri, json) } }
    }

    override fun register(item: SelectableNetwork) {
        val uri = networkUri ?: throw NetworkRepoError(StringsResource.get(NETWORK_URI_ERROR))
        val json = jsonFormat.encodeToString(SelectableNetwork.serializer(), item)
        cache[item.network.id] = item
        runBlocking { launch(IO) { persist(uri, json) } }
    }

    override fun toggleHidden(item: SelectableNetwork) {
        TODO("Not yet implemented")
    }

    override fun toggleSelected(item: SelectableNetwork) = register(item.copy(isSelected = !item.isSelected))

    private fun getKeyFromItem(item: SelectableNetwork) = item.network.id
}

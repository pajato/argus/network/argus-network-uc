package com.pajato.argus.network.uc

import com.pajato.argus.network.core.Network
import com.pajato.argus.network.core.SelectableNetwork
import com.pajato.argus.network.uc.NetworkUseCases.add
import com.pajato.argus.network.uc.NetworkUseCases.get
import com.pajato.argus.network.uc.NetworkUseCases.toggleSelection
import com.pajato.argus.network.uc.TestNetworkRepo.cache
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class ToggleSelectionUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val url = loader.getResource("files/networks.txt") ?: fail("Could not load networks resource file!")
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestNetworkRepo.injectDependency(url.toURI()) }
    }

    @Test fun `When an added item is toggled item, verify behavior`() {
        val item = SelectableNetwork(false, false, Network(0))
        cache.clear()
        add(TestNetworkRepo, item)
        assertEquals(1, cache.size)
        assertToggledItem(true)
        assertToggledItem(false)
    }

    private fun assertToggledItem(value: Boolean) {
        fun getItem(): SelectableNetwork = get(TestNetworkRepo, 0) ?: fail("Item should not be null!")
        toggleSelection(TestNetworkRepo, getItem())
        assertEquals(value, getItem().isSelected)
    }
}

package com.pajato.argus.network.uc

import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class GetAllUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        fun getUrl() = loader.getResource("files/networks.txt") ?: fail("Could not load networks resource file!")
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestNetworkRepo.injectDependency(getUrl().toURI()) }
    }

    @Test fun `When retrieving an item, verify behavior`() {
        val list = NetworkUseCases.getAll(TestNetworkRepo)
        assertEquals(11, list.size, "$list")
    }
}

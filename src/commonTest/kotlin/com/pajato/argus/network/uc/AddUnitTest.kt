package com.pajato.argus.network.uc

import com.pajato.argus.network.core.Network
import com.pajato.argus.network.core.SelectableNetwork
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class AddUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val url = loader.getResource("files/networks.txt") ?: fail("Could not load networks resource file!")
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestNetworkRepo.injectDependency(url.toURI()) }
    }

    @Test fun `When adding an item, verify behavior`() {
        val item = SelectableNetwork(false, false, Network(0))
        TestNetworkRepo.cache.clear()
        NetworkUseCases.add(TestNetworkRepo, item)
        assertEquals(1, TestNetworkRepo.cache.size)
    }
}

package com.pajato.argus.network.uc

import com.pajato.argus.network.core.NetworkRepo
import com.pajato.argus.network.core.SelectableNetwork

public object NetworkUseCases {
    public fun add(repo: NetworkRepo, item: SelectableNetwork): Unit = repo.register(item)

    public fun modify(repo: NetworkRepo, item: SelectableNetwork): Unit = repo.register(item)

    public fun get(repo: NetworkRepo, id: Int): SelectableNetwork? = repo.cache[id]

    public fun getAll(repo: NetworkRepo): List<SelectableNetwork> = repo.cache.values.toList()

    public fun toggleSelection(repo: NetworkRepo, item: SelectableNetwork): Unit = repo.toggleSelected(item)
}
